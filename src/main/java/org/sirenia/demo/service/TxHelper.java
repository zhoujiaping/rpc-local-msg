package org.sirenia.demo.service;

import fn.fn.Fn0;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TxHelper {
    @Autowired
    ApplicationContext app;
    @Transactional(rollbackFor = Throwable.class,propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED,timeout = 5,readOnly = true)
    public <T> T withTxReadOnly(Fn0<T> fn){
        return fn.invoke();
    }
    @Transactional(rollbackFor = Throwable.class,propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED,timeout = 5)
    public <T> T withTx(Fn0<T> fn){
        return fn.invoke();
    }
    @Transactional(rollbackFor = Throwable.class,propagation = Propagation.REQUIRES_NEW,isolation = Isolation.READ_COMMITTED,timeout = 5)
    public <T> T withTxNew(Fn0<T> fn){
        return fn.invoke();
    }
}
