package org.sirenia.demo;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        //SpringApplication.run(App.class);
        SpringApplication app = new SpringApplicationBuilder()
                .sources(App.class)
                //.child(Application.class)
                .bannerMode(Banner.Mode.CONSOLE)
                .build();
        //如果同时依赖了spring-boot-starter-web 和spring-boot-starter-webflux，默认使用springmvc，
        // 需要强制设置web应用类型才能使用webflux
        //app.setWebApplicationType(WebApplicationType.REACTIVE);
        app.addListeners(new ApplicationPidFileWriter("/etc/rpc-local-msg/app.pid"));
        app.run(args);
    }
}
