package org.sirenia.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static fn.Cores.hashMap;

@RestController
public class EchoController {
    @GetMapping("/echo")
    public Object echo() {
        return hashMap("code", "200",
                "msg", "ok");
    }
}
