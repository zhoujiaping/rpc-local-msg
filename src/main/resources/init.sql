drop table local_msg;
-- 一条记录代表一次rpc调用，不管调用成功还是失败。
-- sending：rpc调用还没有发起。
-- completed：rpc调用成功，不管业务上是否成功，只管本次rpc调用是否成功返回结果。
create table local_msg(
    id bigint primary key auto_increment comment '主键',
    biz_type varchar(64) comment '业务类型',
    biz_no varchar(128) comment '业务号',
    content varchar(5000) comment '消息内容',
    status varchar(36) comment 'prepared,committed'
    send_times int comment '发送次数',
    last_send_time datetime default now() comment '最近发送时间',
    create_time datetime default now() comment '创建时间',
    version int default 0 comment '乐观锁版本号',
    valid int default 1 comment '逻辑删除标识。1：有效, 0：无效'
);
alter table local_msg add unique unique_idx(biz_type,biz_no);